#!/usr/bin/env python
#-*-coding:utf-8-*-
#
#       kutucuk.py
#       
#       Copyright 2009 Can Mustafa Ozdemir <canmustafaozdemir [\at/] gmail [-dot-] com>
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from Tkinter import *
import random
from Xlib import display

from pygame import mixer
import os

class Resolution:
    def __init__(self, display):
        self.width = display.screen().width_in_pixels
        self.height = display.screen().height_in_pixels



class tkMain(Frame):
    def __init__(self):
        Frame.__init__(self)
        self.master.overrideredirect(1)
        self.pack()
        self.base()
        screen = Resolution(display.Display())
        self.width = screen.width
        self.height = screen.height
        self.standart_vars()
        mixer.init()
        self.sound1 = mixer.Sound("scratch.wav")
        self.sound2 = mixer.Sound("glass.wav")
        self.sound3 = mixer.Sound("boink.wav")
    
    '''Defining the start up variables'''
    def standart_vars(self):
        self.can1["bg"] = "blue"
        self.x = random.randint(1, self.width-100)
        self.y = random.randint(25, self.height/2)
        self.xhistory = 0
        self.yhistory = 0
        self.end1 = True
        self.end2 = True
        self.score = 0
        self.master.geometry("50x50+%s+%s"%(self.width/2, self.height/2))
        self.command_state = True
    
    '''generate 8 different red boxes'''
    def generate_toplevels(self):
        self.t1 = RedBoxes(self.width, self.height)
        self.t1_var = True
        self.t2 = RedBoxes(self.width, self.height)
        self.t2_var = True
        self.t3 = RedBoxes(self.width, self.height)
        self.t3_var = True
        self.t4 = RedBoxes(self.width, self.height)
        self.t4_var = True
        self.t5 = RedBoxes(self.width, self.height)
        self.t5_var = True
        self.t6 = RedBoxes(self.width, self.height)
        self.t6_var = True
        self.t7 = RedBoxes(self.width, self.height)
        self.t7_var = True
        self.t8 = RedBoxes(self.width, self.height)
        self.t8_var = True
    
    '''if green box reaches to the edges or
    reacted by mouse pointer 
    this code changes its direction
    '''    

    
    def turn_back(self):
        if self.yhistory<self.y:
            self.end2 = False
            self.y -= 20
        else:
            self.end2 = True
            self.y -= 20
        
    def turn_back_event(self, event):
        if self.yhistory<self.y:
            self.end2 = False
            self.y -= 20
        if self.yhistory==self.y:
            self.end2 = True
            self.y -= 20

    '''
    Basic engine
    '''
    def move(self):
        self.xhistory = self.x
        self.yhistory = self.y
        if self.x > self.width-50:
            self.end1 = False
            self.sound3.play()
        if self.x <= 0:
            self.end1=True
            self.sound3.play()
        if self.y<=0:
            self.end2=True
            self.sound3.play()
        if self.end1==True:
            self.x+=5
        else:
            self.x-=5
        if self.end2==True:
            self.y+=5
        else:
            self.y-=5

        if (self.x>self.t1.tx-50 and self.x<self.t1.tx+50) and \
          (self.y>self.t1.ty-50 and self.y<self.t1.ty+50) and self.t1_var==True:
            self.sound1.play()
            self.t1_var=False
            self.score = self.score + self.t1.points
            self.t1.destroy() 
            self.turn_back()
        if (self.x>self.t2.tx-50 and self.x<self.t2.tx+50) and \
          (self.y>self.t2.ty-50 and self.y<self.t2.ty+50) and self.t2_var==True:
            self.sound1.play()
            self.t2_var=False
            self.score = self.score + self.t2.points
            self.t2.destroy() 
            self.turn_back()
        if (self.x>self.t3.tx-50 and self.x<self.t3.tx+50) and \
          (self.y>self.t3.ty-50 and self.y<self.t3.ty+50) and self.t3_var==True:
            self.sound1.play()
            self.t3_var=False
            self.score = self.score + self.t3.points
            self.t3.destroy() 
            self.turn_back()
        if (self.x>self.t4.tx-50 and self.x<self.t4.tx+50) and \
          (self.y>self.t4.ty-50 and self.y<self.t4.ty+50) and self.t4_var==True:
            self.sound1.play()
            self.t4_var=False
            self.score = self.score + self.t4.points
            self.t4.destroy() 
            self.turn_back()
        if (self.x>self.t5.tx-50 and self.x<self.t5.tx+50) and \
          (self.y>self.t5.ty-50 and self.y<self.t5.ty+50) and self.t5_var==True:
            self.sound1.play()
            self.t5_var=False
            self.score = self.score + self.t5.points
            self.t5.destroy()
            self.turn_back()
        if (self.x>self.t6.tx-50 and self.x<self.t6.tx+50) and \
          (self.y>self.t6.ty-50 and self.y<self.t6.ty+50) and self.t6_var==True:
            self.sound1.play()
            self.t6_var=False
            self.score = self.score + self.t6.points
            self.t6.destroy()    
            self.turn_back()    
        if (self.x>self.t7.tx-50 and self.x<self.t7.tx+50) and \
          (self.y>self.t7.ty-50 and self.y<self.t7.ty+50) and self.t7_var==True:
            self.sound1.play()
            self.t7_var=False
            self.score = self.score + self.t7.points
            self.t7.destroy()    
            self.turn_back()
        if (self.x>self.t8.tx-50 and self.x<self.t8.tx+50) and \
          (self.y>self.t8.ty-50 and self.y<self.t8.ty+50) and self.t8_var==True:
            self.t8_var=False
            self.score = self.score + self.t8.points
            self.t8.destroy()    
            self.turn_back()
        self.can1.delete("all")
        self.can1.create_text(20,20, text="%s\npts."%self.score, fill="red")
        if self.t1_var == False and self.t2_var == False and \
          self.t3_var == False and self.t4_var == False and \
          self.t5_var == False and self.t6_var == False and \
          self.t7_var == False and self.t8_var == False:
            self.generate_toplevels()
        
        
        self.master.geometry("50x50+%s+%s"%(self.x, self.y))
        if self.y > self.height:
            self.after_cancel(self.loop)
            self.sound2.play()
            self.can1["bg"] = "blue"
            self.can1.delete("all")
            self.can1.create_text(20,30, text="%s\nPts."%self.score, fill="white")
            self.command_state = True
            for i in range(10):
                WhiteBoxes(self.x+random.randint(-50,0), self.y+random.randint(-50,0))
            self.master.geometry("50x50+%s+%s"%(self.width/2, self.height/2))
            
            
        else:
            self.loop = self.after(10, self.move)
    
    def start_move(self):
        if self.command_state == True:
            self.standart_vars()
            self.can1["bg"] = "green"
            self.can1.delete("all")
            self.command_state = False
            try:
                if self.t1_var == False and self.t2_var == False and \
                  self.t3_var == False and self.t4_var == False and \
                  self.t5_var == False and self.t6_var == False and \
                  self.t7_var == False and self.t8_var == False:
                    self.generate_toplevels()
            except:
                self.generate_toplevels()
            self.move()
    
    def move_event(self, event):
        self.start_move()
    
    def close(self, event):
        self.master.quit()
    '''Base widgets'''
    
    def close_button_animate1(self, event):
        self.can2.delete("all")
        self.can2.config(width=15, height=15)
        self.can2.create_line(0,15, 15, 0, fill="white")
        self.can2.create_line(0, 0, 15, 15, fill="white")
    def close_button_animate2(self, event):
        self.can2.delete("all")
        self.can2.config(width=10, height=10)
        self.can2.create_line(0,10, 10, 0, fill="white")
        self.can2.create_line(0, 0, 10, 10, fill="white")
    
    def locate_box(self, event):
        self.master.geometry("50x50+%s+%s"%(event.x_root-20, event.y_root-20))
    
    def base(self):
        self.can1 = Canvas(bg="blue")
        self.can1.bind("<Button-1>", self.move_event)
        self.can1.bind("<Motion>", self.turn_back_event)
        self.can1.bind("<B2-Motion>", self.locate_box)
        self.can1.pack()
        self.can1.create_text(20,20, text="Click\nme!", fill="white")
        self.can2 = Canvas(self.can1, bg="red", width=10, height=10)
        self.can2.create_line(0,10, 10, 0, fill="white")
        self.can2.create_line(0, 0, 10, 10, fill="white")
        self.can2.place(x=35, y=0)
        self.can2.bind("<Button-1>", self.close)
        self.can2.bind("<Enter>", self.close_button_animate1)
        self.can2.bind("<Leave>", self.close_button_animate2)
        self.right_click_menu(self.master)
        self.can1.bind("<Button-3><ButtonRelease-3>", self.start_menu)
    
    def right_click_menu(self, t):
        self.menu = Menu(t, tearoff=0)
        self.menu.add_command(label="Start", command=self.start_move)
        self.menu.add_command(label="Cheat")
    def start_menu(self, z):
        self.right_click_menu(self.master)
        t = z.widget
        self.menu.tk.call("tk_popup", self.menu, z.x_root, z.y_root)
    
class RedBoxes(Toplevel):
    '''
    Red Boxes
    '''
    def __init__(self, width, height):
        Toplevel.__init__(self)
        self["bg"] = "red"
        self.points = random.randint(1, 1000)
        self.lab = Label(self, text="%s\npts."%self.points, bg="red", fg="white")
        self.lab.place(x=10, y=10)
        self.tx = random.randint(0, width-50)
        self.ty = random.randint(0, height-50)
        self.geometry("50x50+%s+%s"%(self.tx, self.ty))
        self.overrideredirect(1)

class WhiteBoxes(Toplevel):
    '''
    Small white boxes for the explosion animation
    when green box reaches the bottom
    '''
    def __init__(self, atx, aty):
        Toplevel.__init__(self)
        self["bg"] = "white"
        self.atx = atx
        self.aty = aty
        self.timer = 0
        self.xdirection = random.randint(-5, 5)
        self.ydirection = random.randint(-5, 5)
        self.geometry("10x10+%s+%s"%(self.atx, self.aty))
        self.overrideredirect(1)
        self.move()
    def move(self):
        self.timer += 1
        self.atx +=self.xdirection
        self.aty +=self.ydirection
        self.geometry("10x10+%s+%s"%(self.atx, self.aty))
        if self.timer>50:
            self.after_cancel(self.loop)
            self.destroy()
        else:
            self.loop = self.after(10, self.move)    
        
if __name__ == "__main__":
    app = tkMain()
    app.mainloop()
